##### UDPCAST

Una opció molt interessant perquè un gran nombre de peticions d'un fitxer de mida gran no saturin la xarxa és fer servir `udpcast` paquet que conté els binaris `udp-sender` i `udp-receiver`. Per defecte, `udpcast` fa servir multicast: d'aquesta manera només s'envien els paquets a aquells receptors que ho demanin. També es podria canviar aquest comportament i fer servir `broadcast` (si tenim un *switch* a la xarxa la millora és evident, amb multicast el switch no enviarà els paquets a totes les *boques*).

El host que envia el fitxer:

```
udp-sender --file fitxer.iso
```

El host que rep el fitxer:

```
udp-receiver --file fitxer.iso
```

Un cop han executat els receptors esperats la instrucció, qui envia el fitxer prem una tecla i comença la transmissió.

En principi la idea és que qui envia sigui el primer que executa la instrucció
i els receptors executin després les instruccions. Si ho fan be, quan l'emissor
veu que ja hi ha el número de receptors que espera, prem una tecla i comença
l'enviament. El problema és que moltes vegades els receptors s'equivoquen i
premen una tecla de més, llavors comencen ells la transmissió. per resoldre
aixó hi ha diiferents opcions.

* Que el receptor posi l'opció --nokbd
* Que l'emissor posi l'opció --autostart n (on n és el nombre de receptors)
* Que l'emissor no executi l'ordre fins que l'executin els receptors.

