### Enunciats exercicis gestió de paquets i repositoris en distribucions Debian
 
Instal·lar software amb `aptitude`, `apt-get`, `dpkg`

##### Exercici 1

Quin és el fitxer de configuració dels repositoris (amb trajectòria completa)?
Descriu les diferents parts d'una línia d'aquest fitxer.

```
deb http://site.example.com/debian distribution component1 component2 component3
```

Si volguéssim que la nostra distro fos 100% lliure quin/quins serien els components?

##### Exercici 2

A les distribucions derivades de RedHat-Fedora l'ordre `dnf upgrade` (antigament a Fedora i a Centos ara: `yum update`) ens actualitza tots els paquets del sistema, quina/es ordres farien el mateix en una distribució tipus Debian?

##### Exercici 3

Instal·leu clementine, vlc, gimp, shotwell, gparted, gnome-disk-utility, libreoffice-writer, libreoffice-calc, libreoffice-impress, xournal, pgmodeler i aptitude

Trobeu algun paquet pel que sigui necessari fer servir *contrib* i/o
*non-free*.

##### Exercici 4

Volem afegir el repositori de *Riot*, per a això podríem afegir una línia al
final del fitxer de configuració d'abans, o fer un nou fitxer per a aquest
repositori. Optarem per aquesta 2a opció. [Ajuda](https://riot.im/desktop.html)
 
Seguiu les recomanacions de l'enllaç i actualitzeu el repositori, importeu les claus que garanteixen que el programa és de fonts fiables i finalment instal·leu el programa.

##### Exercici 5

Amb aptitude, en mode *interactiu*, instal·la i desinstal·la `mc` (o al revés si ja estava instal·lat) 

##### Exercici 6

El concepte de package group (grup de paquets) de Fedora com es diu a Debian ? 

##### Exercici 7

Quin o quines ordres ens permeten instal·lar aquestes tasques ? 

##### Exercici 8

Com puc llistar les possibles tasques de que disposo ? 

##### Exercici 9

Mostra amb `dpkg` el contingut del paquet xterm? I tots els paquets instal·lats, amb la mateixa ordre?

##### Exercici 10

Tradueix del "llenguatge RPM" al "llenguatge DEB" les següents instruccions: 

dnf upgrade  ----> 
dnf downgrade foo-1.0 (downgrade el paquet foo a la versió 1.0) ---->
dnf list --installed  ----->
dnf search  paquet ---->
rpm -ql paquet  (o repoquery -l) ----> 
rpm -qf /bin/ping  (o dnf provides */ping)  ---->

##### Exercici 11

Que fa la següent instrucció ? 

```
aptitude -v moo 
```

i aquesta ? 

```
aptitude -vv moo
```

i aquesta ? 

```
aptitude -vvv moo
```
 
OBS: Per fer aquest exercicis és molt recomanable utilitzar els man corresponents o alguns enllaços com per exemple:

[Package Management Basics: apt, yum, dnf, pkg](https://www.digitalocean.com/community/tutorials/package-management-basics-apt-yum-dnf-pkg)

