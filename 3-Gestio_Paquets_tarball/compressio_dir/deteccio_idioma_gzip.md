### Detecció de idioma en un fitxer de text utilitzant gzip

De les moltes maneres que hi ha d'intentar detectar l'idioma d'un fitxer de text de manera automàtica aquesta és molt interessant:
[youtube](https://www.youtube.com/watch?v=dIEStL7riMo)

Una explicació més detallada es troba en [aquest enllaç](https://ileriseviye.wordpress.com/2011/12/19/language-detection-in-the-bash-command-line-using-gzip-aka-comprehension-is-compression/)





Tan sols tenir en compte una petit problema: el darrer enllaç utilitza un fitxer/URL que ja no existeix

http://www.gutenberg.org/ebooks/46.txt.utf8 i que el podeu substituir per aquest http://www.gutenberg.org/cache/epub/46/pg46.txt

De manera que l'ordre quedaria com:

```
wget http://www.gutenberg.org/cache/epub/46/pg46.txt && mv pg46.txt EN && wget http://www.gutenberg.org/cache/epub/2229/pg2229.txt && mv pg2229.txt DE
```

