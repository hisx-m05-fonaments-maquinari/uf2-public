### Introducció a la compressió


###### Problema

*Podem reduir la mida en bytes d'un fitxer sense perdre informació?*

Per poder respondre a aquesta pregunta, s'ha d'estudiar el tipus d'informació que conté el fitxer, comprovar si hi ha patrons que es repeteixen, dissenar un algoritme que codifiqui i decodifiqui de manera única ...

###### Exemple senzill de compressió

El fitxer [file1.txt](file1.txt) conté el següent text:
> Ask not what your country can do for you -- ask what you can do for your country.

Podem fer un petit diccionari amb les paraules del fitxer:

```
ask what your country can do for you
```

Aquestes paraules estan totes repetides, excepte *not* totes surten 2 cops a la frase original. Si tenim en compte l'ordre en el qual surten les paraules repetides i assignem números:

```
1	2	3	...
ask	what	your	...
```

Podriem codificar la informació de la següent manera:
```
1 not 2 3 4 5 6 7 8 -- 1 2 8 5 6 7 3 4
```

Aquesta codificació la trobarem al fitxer [file2.txt](file2.txt)

Què hem aconseguit? Hem passat de 81 bytes a 75. No sembla molt però sense fer un treball gaire sofisticat ens hem estalviat més d'un 7%.

###### Exemple de compressió un xic més sofisticat

La compressió anterior és interessant però els programes de compressió no funcionen com les persones, per a nosaltres és més fàcil pensar en els patrons utilitzant paraules però els compressors no fan això.

Observem detenidament la frase buscant patrons repetits, tot i que no siguin paraules.

Per exemple, ara podem fer un diccionari una mica més complex, estarà format per un conjunt de paraules i espais en blanc (el qual representarem pel caràcter `_`).

En efecte, el nostre nou diccionari estarà format pels següents patrons:

```
1	2	3	4		5
ask_	what_	you	r_country  	_can_do_for_you
```

Un altre cop estem dient que *ask_* (en realitat "ask ") és el 1er patró del noste nou diccionari, *what_* serà el 2on patró ...

De manera que:

```
1not_2345_--_12354
```

mostra la famosa frase de JFK codificada. 

I ara hem passat dels 81 bytes originals a 60 ([file3.txt](file3.txt)): més d'un 25% d'estalvi en una estoneta!!!
Que no podríem fer pensant molt més de temps?

Extret d'aquest [enllaç](http://computer.howstuffworks.com/file-compression.html)



###### CURIOSITATS

Algunes conseqüències interessants d'aquest algoritmes és que es poden fer servir per tasques que a priori semblarien molt allunyades del motiu pel qual es van dissenyar: com pot ser [la detecció automàtica amb `gzip` de l'idioma en el qual està escrit un text](deteccio_idioma_gzip.md).

